# Localizer

This is utility for working with localization files on Swift (support localization prom .plist, .json)

### Requirements
 - Swift 5 / Xcode 10.2

### Installation

```bash
$ brew tap bestq/formulaes git@bitbucket.org:best-q/formulaes.git
$ brew install localizer
```

### Commands

  - `gen-swift-struct` - Create struct with locales from json/plist file.
                  --P - make public
                Params: `-P [json/plist file path] [struct file path]`
  - `gen-resource-files` - Create Localizable.strings resources
                     Params: `-P [json/plist file path] [localization path] [filename default is Localizable.strings]`
 - `convert`  - Convert input localization to output file path with selected type.
                Params: `[input file] [output file]`
 - `load`     - Get language table data from Google Sheets by selected config and save as JSON file.
                If configuration file is not specified in parameters, it will be taken from CWD as 'sheet_config.json'.
                For sample configuration file see this, note that spreadsheet should be share by link:
```
{
  "credentials": {
    <Google API JSON model from https://console.cloud.google.com/apis/credentials>"
  },
  "spreadsheetId": "<Spreadsheet ID>",
  "listName": "<Page name>",
  "columns": {                      #
    "exclude": "exclude",           # as default
    "key": "key",                   #
    "platform": "platform",         #
    "quantity": "quantity",         #
    "spannable": "spannable",       # (values will be used if not specified)
    "comment": "comment"            #
  },                                #
  "currentPlatformName": "ios",     #
  "commonPlatformName": "common"    #
}
```

 - [Providing service credentials and account manually](https://cloud.google.com/docs/authentication/production#obtaining_and_providing_service_account_credentials_manually) for more information about generating **API key**
 - [Google Sheets API guides](https://developers.google.com/sheets/api/guides/concepts) for more information about **Spreadsheet ID**
