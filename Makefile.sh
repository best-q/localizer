#!/bin/sh

#  Makefile.sh
#  localizer
#
#  Created by Admin.
#  Copyright © 2020 Admin. All rights reserved.

SHELL = /bin/bash

prefix ?= /usr/local
bindir ?= $(prefix)/bin

REPODIR = $(shell pwd)
BUILDDIR = $(REPODIR)/.build

.DEFAULT_GOAL = all

.PHONY: all
all: localizer

localizer:
	@swift build \
		-c release \
		--disable-sandbox \
		--build-path "$(BUILDDIR)"

.PHONY: install
install: localizer
	@install -d "$(bindir)"
	@install "$(BUILDDIR)/release/localizer" "$(bindir)"

.PHONY: uninstall
uninstall:
	@rm -rf "$(bindir)/localizer"

.PHONY: clean
distclean:
	@rm -f $(BUILDDIR)/release

.PHONY: clean
clean: distclean
	@rm -rf $(BUILDDIR)
