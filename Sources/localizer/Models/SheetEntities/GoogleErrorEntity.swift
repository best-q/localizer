//
//  GoogleErrorEntity.swift
//  localizer
//
//  Created by Admin.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

struct GoogleErrorEntity: Codable {
	let error: GoogleErrorItem
}

struct GoogleErrorItem: Codable {
	let code: Int?
	let message, status: String?
	let details: [GoogleErrorDetailItem]?

	var description: String {
		return [String(describing: code), status, message, String(describing: details)]
			.compactMap({ $0 })
			.joined(separator: " - ")
	}
}

struct GoogleErrorDetailItem: Codable {
	let type: String
	let links: [GoogleErrorLinkItem]

	enum CodingKeys: String, CodingKey {
		case type = "@type"
		case links
	}
}

struct GoogleErrorLinkItem: Codable {
	let linkDescription: String
	let url: String

	enum CodingKeys: String, CodingKey {
		case linkDescription = "description"
		case url
	}
}
