//
//  GoogleCredentialsEntity.swift
//  localizer
//
//  Created by Admin.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation
import SwiftJWT

struct GoogleCredentialsEntity: Codable {
	struct GoogleClaims: Claims {
		let iss: String
		let scope: String
		let aud: String
		let exp: Date
		let iat: Date
	}

	let type, projectID, privateKeyID, privateKey: String
	let clientEmail, clientID, authURI, tokenURI: String
	let authProviderX509CERTURL, clientX509CERTURL: String

	enum CodingKeys: String, CodingKey {
		case type
		case projectID = "project_id"
		case privateKeyID = "private_key_id"
		case privateKey = "private_key"
		case clientEmail = "client_email"
		case clientID = "client_id"
		case authURI = "auth_uri"
		case tokenURI = "token_uri"
		case authProviderX509CERTURL = "auth_provider_x509_cert_url"
		case clientX509CERTURL = "client_x509_cert_url"
	}

	func getAccessToken() -> String {
		let claims = GoogleClaims(
			iss: clientEmail,
			scope: Constants.googleScopes.joined(separator: " "),
			aud: Constants.googleTokenUrl,
			exp: Date(timeIntervalSinceNow: Constants.tokenExpiredTime),
			iat: Date(timeIntervalSinceNow: 0)
		)
		let myJWT = JWT(header: Header(), claims: claims)
		guard let privateKeyData = privateKey.data(using: .utf8) else {
			print(error: "Private key converting error.")
			exit(EXIT_FAILURE)
		}
		let jwtSigner = JWTSigner.rs256(privateKey: privateKeyData)
		let jwtEncoder = JWTEncoder(jwtSigner: jwtSigner)
		do {
			return try jwtEncoder.encodeToString(myJWT)
		}
		catch {
			print(error: "Token encode error: \(error.localizedDescription)")
			exit(EXIT_FAILURE)
		}
	}
}
