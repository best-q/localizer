//
//  SheetEnteties.swift
//  localizer
//
//  Created by Admin.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

struct SheetInfo: Codable {
	var sheets: [SheetInfoItem]
}

struct SheetInfoItem: Codable {
	var properties: SheetInfoProperties
}

struct SheetInfoProperties: Codable {
	var sheetId: Int
	var title: String
	var index: Int
	var sheetType: String
	var gridProperties: SheetInfoGridProperties
}

struct SheetInfoGridProperties: Codable {
	var rowCount: Int
	var columnCount: Int
}

struct SheetData: Codable {
	var range: String
	var majorDimension: String
	var values: [[String]]
}
