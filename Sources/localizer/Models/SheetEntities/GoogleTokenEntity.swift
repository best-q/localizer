//
//  GoogleTokenEntity.swift
//  localizer
//
//  Created by Admin.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

struct GoogleTokenEntity: Codable {
	let accessToken: String
	let expiresIn: Int
	let tokenType: String

	enum CodingKeys: String, CodingKey {
		case accessToken = "access_token"
		case expiresIn = "expires_in"
		case tokenType = "token_type"
	}
}
