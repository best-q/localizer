//
//  SheetEntity.swift
//  localizer
//
//  Created by Admin.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

class SheetEntity: Codable {

	enum CodingKeys: String, CodingKey {
		case credentials
		case spreadsheetId
		case listName
		case columns
		case commonPlatformName
		case currentPlatformName
	}

	let credentials: GoogleCredentialsEntity
	let spreadsheetId: String
	let listName: String
	let columns: SheetColumnsEntity
	let commonPlatformName: String
	let currentPlatformName: String

	required init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		self.credentials = try container.decode(GoogleCredentialsEntity.self, forKey: .credentials)
		self.spreadsheetId = try container.decode(String.self, forKey: .spreadsheetId)
		self.listName = try container.decode(String.self, forKey: .listName)
		self.columns = try container.decodeKey(key: .columns, defaultValue: SheetColumnsEntity())
		self.commonPlatformName = try container.decodeKey(key: .commonPlatformName, defaultValue: "common")
		self.currentPlatformName = try container.decodeKey(key: .currentPlatformName, defaultValue: "ios")
	}

}

class CSVTableEntity: Codable {
    enum CodingKeys: String, CodingKey {
        case columns
        case commonPlatformName
        case currentPlatformName
    }

    let columns: SheetColumnsEntity
    let commonPlatformName: String
    let currentPlatformName: String

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.columns = try container.decodeKey(key: .columns, defaultValue: SheetColumnsEntity())
        self.commonPlatformName = try container.decodeKey(key: .commonPlatformName, defaultValue: "common")
        self.currentPlatformName = try container.decodeKey(key: .currentPlatformName, defaultValue: "ios")
    }
}

class SheetColumnsEntity: Codable {

	enum CodingKeys: String, CodingKey {
		case exclude
		case key
		case platform
		case quantity
		case spannable
		case comment
	}

	let exclude: String
	let key: String
	let platform: String
	let quantity: String
	let spannable: String
	let comment: String

	init() {
		self.key = CodingKeys.key.rawValue
		self.platform = CodingKeys.platform.rawValue
		self.exclude = CodingKeys.exclude.rawValue
		self.quantity = CodingKeys.quantity.rawValue
		self.spannable = CodingKeys.spannable.rawValue
		self.comment = CodingKeys.comment.rawValue
	}

    required init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		self.key = try container.decodeKey(key: .key)
		self.platform = try container.decodeKey(key: .platform)
		self.exclude = try container.decodeKey(key: .exclude)
		self.quantity = try container.decodeKey(key: .quantity)
		self.spannable = try container.decodeKey(key: .spannable)
		self.comment = try container.decodeKey(key: .comment)
	}

	func getAll() -> [String] {
		return [exclude, key, platform, quantity, spannable, comment]
	}
}
