//
//  LocalizationEntity.swift
//  localizer
//
//  Created by Admin.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

protocol LocalizationEntity: Codable {
	var fullName: String { get }
	var comment: String? { get }

	func merged(with newItem: LocalizationEntity) -> LocalizationEntity
}
extension LocalizationEntity {
	var name: String {
		return String(fullName.split(separator: Constants.delimeter).last ?? "")
	}
}

enum PluralType: String, Codable {
	case zero, one, two, few, many, other
}

struct PluralEntity: LocalizationEntity, Codable, Hashable {
	let types: [PluralType: String]
	let fullName: String
	let spannable: Bool
	let comment: String?

	enum CodingKeys: CodingKey {
		case fullName, types, spannable, comment
	}

	init(from decoder: Decoder) throws {
		let singleContainer = try decoder.singleValueContainer()
		let namePath = singleContainer.codingPath.dropFirst(3)
		let fullname = namePath.map({$0.stringValue}).joined(separator: String(Constants.delimeter))


		if let value = try? singleContainer.decode([String: String].self) {
			var plural: [PluralType: String] = [:]
			value.forEach {
				guard let type = PluralType(rawValue: $0.key) else {
					fatalError("Unssuported plural type for key \($0.key)")
				}
				plural[type] = $0.value

			}
			self.types = plural
			self.comment = nil
			self.fullName = fullname
			self.spannable = false
			return
		}

		let container = try decoder.container(keyedBy: CodingKeys.self)

		let spannable = try? container.decode(Bool.self, forKey: .spannable)
		let comment = try? container.decode(String.self, forKey: .comment)

		let dict = try container.decode([String: String].self, forKey: .types)
		var plural: [PluralType: String] = [:]
		dict.forEach {
			guard let type = PluralType(rawValue: $0.key) else {
				fatalError("Unssuported plural type \($0.key)")
			}
			plural[type] = $0.value

		}
		self.types = plural
		self.comment = comment
		self.fullName = fullname
		self.spannable = spannable ?? false

	}

	init(fullName: String, types: [PluralType: String], spannable: Bool, comment: String?) {
		self.fullName = fullName
		self.types = types
		self.spannable = spannable
		self.comment = comment
	}

	func merged(with newItem: LocalizationEntity) -> LocalizationEntity {
		var types: [PluralType: String] = (newItem as? PluralEntity)?.types ?? [: ]
		types = types.merging(self.types, uniquingKeysWith: {(current, _) in current })
		return PluralEntity(fullName: fullName, types: types, spannable: spannable, comment: comment)
	}

	func encode(to encoder: Encoder) throws {
		var container = encoder.container(keyedBy: CodingKeys.self)
		var hasAnotherKey = false
		if spannable != false {
			try container.encode(spannable, forKey: .spannable)
			hasAnotherKey = true
		}
		if let comment = comment, !comment.isEmpty {
			try container.encode(comment, forKey: .comment)
			hasAnotherKey = true
		}

		var pluralContainer = hasAnotherKey ? container.nestedContainer(keyedBy: DynamicIntStringKey.self, forKey: .types) : encoder.container(keyedBy: DynamicIntStringKey.self)
		for item in types.enumerated() {
			let key = DynamicIntStringKey(stringValue: item.element.key.rawValue)!
			try pluralContainer.encode(item.element.value, forKey: key)
		}
	}
}

struct StringEntity: LocalizationEntity, Codable, Hashable {
	let fullName: String
	let value: String
	let spannable: Bool
	let comment: String?

	enum CodingKeys: CodingKey {
		case fullName, value, spannable, comment
	}

	init(fullName: String, value: String, spannable: Bool, comment: String?) {
		self.fullName = fullName
		self.value = value
		self.spannable = spannable
		self.comment = comment
	}

	init(from decoder: Decoder) throws {
		let singleContainer = try decoder.singleValueContainer()
		let namePath = singleContainer.codingPath.dropFirst(3)

		let fullname = namePath.map({$0.stringValue}).joined(separator: String(Constants.delimeter))

		if let value = try? singleContainer.decode(String.self) {
			self.fullName = fullname
			self.value = value
			self.spannable = false
			self.comment = nil
			return
		}
		let container = try decoder.container(keyedBy: CodingKeys.self)

		let comment = try? container.decode(String.self, forKey: .comment)
		let value = try container.decode(String.self, forKey: .value)
		let spannable = try? container.decode(Bool.self, forKey: .spannable)

		self.fullName = fullname
		self.value = value
		self.spannable = spannable ?? false
		self.comment = comment
	}

	func merged(with newItem: LocalizationEntity) -> LocalizationEntity {
		return newItem
	}

	func encode(to encoder: Encoder) throws {
		if spannable == false, (comment == nil || comment!.isEmpty) {
			var container = encoder.singleValueContainer()
			try container.encode(value)
		}
		else {
			var container = encoder.container(keyedBy: CodingKeys.self)
			try container.encode(value, forKey: .value)
			if spannable != false {
				try container.encode(spannable, forKey: .spannable)
			}
			if let comment = comment, !comment.isEmpty {
				try container.encode(comment, forKey: .comment)
			}
		}
	}
}
