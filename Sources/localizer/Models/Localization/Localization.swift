//
//  Localization.swift
//  localizer
//
//  Created by Admin.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

struct Localization: Codable {
	let languages: [Language]
}

struct Language: Codable {
	enum CodingKeys: CodingKey {
		case name, isDefault, strings, plurals
	}

	let name: String
	let isDefault: Bool
	let strings: [LocalizationSequence<StringEntity>]
	let plurals: [LocalizationSequence<PluralEntity>]

	init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		self.name = try container.decode(String.self, forKey: .name)
		self.isDefault = try container.decode(Bool.self, forKey: .isDefault)
		self.strings = Array(try container.decode([String: LocalizationSequence<StringEntity>].self, forKey: .strings).values)
		self.plurals = Array(try container.decode([String: LocalizationSequence<PluralEntity>].self, forKey: .plurals).values)
	}

	init(name: String, isDefault: Bool, strings: [LocalizationSequence<StringEntity>], plurals: [LocalizationSequence<PluralEntity>]) {
		self.name = name
		self.isDefault = isDefault
		self.strings = strings
		self.plurals = plurals
	}

	func encode(to encoder: Encoder) throws {
		var container = encoder.container(keyedBy: CodingKeys.self)
		try container.encode(name, forKey: .name)
		try container.encode(isDefault, forKey: .isDefault)
		try container.encode(Dictionary(uniqueKeysWithValues: strings.map{ ($0.name, $0) }), forKey: .strings)
		try container.encode(Dictionary(uniqueKeysWithValues: plurals.map{ ($0.name, $0) }), forKey: .plurals)
	}
}
