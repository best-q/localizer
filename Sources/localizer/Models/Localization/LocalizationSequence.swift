//
//  LocalizationSequence.swift
//  localizer
//
//  Created by Admin.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

struct LocalizationSequence<T>: Codable where T: LocalizationEntity {
	
	enum LocalizationSequenceError: Error {
		case unsupportedFormat
	}
	
	enum CodingKeys: CodingKey {
		case name, children, resources
	}
	
	let name: String
	var children: [LocalizationSequence] = []
	var resources: [LocalizationEntity] = []
	
	init(from decoder: Decoder) throws {
		
		let container = try decoder.container(keyedBy: DynamicIntStringKey.self)
		guard let lastKey = container.codingPath.last else {
			throw LocalizationSequenceError.unsupportedFormat
		}
		
		self.name = lastKey.stringValue
		
		for key in container.allKeys {
			if T.self == StringEntity.self, let resource = try? container.decode(StringEntity.self, forKey: key) {
				self.resources.append(resource)
			}
			else if T.self == PluralEntity.self, let resource = try? container.decode(PluralEntity.self, forKey: key) {
				self.resources.append(resource)
			}
			else {
				let child = try container.decode(LocalizationSequence.self, forKey: key)
				self.children.append(child)
			}
		}
		
	}
	
	init(name: String, children: [LocalizationSequence], resource: LocalizationEntity? = nil ) {
		self.name = name
		self.children = children
		if let resource = resource {
			self.resources = [resource]
		}
		else {
			self.resources = []
		}
	}
	
	func encode(to encoder: Encoder) throws {
		for child in children {
			let key = DynamicIntStringKey(stringValue: child.name)!
			var container = encoder.container(keyedBy: DynamicIntStringKey.self)
			try container.encode(child, forKey: key)
		}
		for res in resources {
			let key = DynamicIntStringKey(stringValue: res.name)!
			var container = encoder.container(keyedBy: DynamicIntStringKey.self)
			if let res = res as? StringEntity {
				try container.encode(res, forKey: key)
			}
			if let res = res as? PluralEntity {
				try container.encode(res, forKey: key)
			}
		}
	}
	
	mutating func merge(sequence: LocalizationSequence) -> Bool {
		guard sequence.name == name else {
			return false
		}
		var dict: [String: LocalizationEntity] = [: ]
		for item in resources + sequence.resources {
			if let oldItem = dict[item.fullName] {
				dict[item.fullName] = oldItem.merged(with: item)
			}
			else {
				dict[item.fullName] = item
			}
		}
		
		resources = Array(dict.values)
		for newChild in sequence.children {
			var isMerged = false
			for (index, var child) in children.enumerated() {
				if child.merge(sequence: newChild) {
					children[index] = child
					isMerged = true
					break
				}
			}
			if !isMerged {
				children.append(newChild)
			}
		}
		return true
	}
	
	static func create<T>(resource: T) -> LocalizationSequence where T: LocalizationEntity {
		let parts = resource.fullName.split(separator: Constants.delimeter).reversed()
		
		guard parts.count >= 2 else {
			fatalError("Could not create sequence and resource with name \(resource.fullName)")
		}
		var sequence: LocalizationSequence!
		
		parts.dropFirst().enumerated().forEach { (offset, partName) in
			if offset == 0 {
				sequence = LocalizationSequence(name: String(partName), children: [], resource: resource)
			}
			else {
				sequence = LocalizationSequence(name: String(partName), children: [sequence])
			}
		}
		return sequence
	}
}
