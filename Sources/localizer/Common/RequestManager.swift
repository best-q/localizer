//
//  RequestManager.swift
//  localizer
//
//  Created by Admin.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

class RequestManager {
	enum Method: String {
		case get = "GET"
		case post = "POST"
	}

	static func create(_ method: Method, to path: URL, Params: [String: Any]? = nil, contentType: String? = nil) -> URLRequest {
		var request = URLRequest(url: path, timeoutInterval: 30.0)
		request.httpMethod = method.rawValue
		request.httpBody = Params?.percentEscaped().data(using: .utf8)
		if let contentType = contentType {
			request.setValue(contentType, forHTTPHeaderField: "Content-Type")
		}
		return request
	}
}
