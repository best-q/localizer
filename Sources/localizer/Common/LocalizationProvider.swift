//
//  LocalizationProvider.swift
//  localizer
//
//  Created by Admin.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

class LocalizationProvider {
	enum Format: String {
		case plist
		case json
	}
	
	func load(filePath: URL) -> Result<(Format, Localization), Error> {
		do {
			let data = try Data(contentsOf: filePath)
			guard let model = try? JSONDecoder().decode(Localization.self, from: data) else {
				let plistModel = try PropertyListDecoder().decode(Localization.self, from: data)
				return .success((.plist, plistModel))
			}
			return .success((.json, model))
		}
		catch {
			return .failure(error)
		}
	}
	
	func save(model: Localization, format: Format, file: URL) -> Result<Void, Error> {
		var data: Data
		do {
			switch format {
			case .json:
				data = try JSONEncoder().encode(model)
			case .plist:
				data = try PropertyListEncoder().encode(model)
			}
			try data.write(to: file)
			return .success(())
		}
		catch {
			return .failure(error)
		}
	}
}
