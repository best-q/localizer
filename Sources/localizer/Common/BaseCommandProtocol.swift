//
//  BaseCommandProtocol.swift
//  localizer
//
//  Created by Admin.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

protocol ConsoleCommandProtocol {
	var name: String { get }
	var description: String { get }
	func perform(arguments: [String])
}
