//
//  DecodeKeyHelper.swift
//  localizer
//
//  Created by Admin.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

extension KeyedDecodingContainer {
	func decodeKey<T>(key: K, defaultValue: T) throws -> T
		where T: Decodable {
			return try decodeIfPresent(T.self, forKey: key) ?? defaultValue
	}

	func decodeKey(key: K) throws -> String {
		return try decodeIfPresent(String.self, forKey: key) ?? key.stringValue
	}
}
