//
//  Color.swift
//  localizer
//
//  Created by Admin.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

public protocol ModeCode {
	var value: UInt8 { get }
}

public enum Color: UInt8, ModeCode {
	case red
	case green
	case yellow
	case blue
	case magenta
	case cyan
	case white
	case lightRed
	case lightGreen
	case lightYellow
	case lightBlue
	case lightMagenta
	case lightCyan
	case lightWhite
	case `default` = 39
	case lightBlack = 90
	case black = 30

	public var value: UInt8 {
		return rawValue
	}
}
extension String {
	func color(_ color: Color) -> String {
		return "\u{001B}[0;\(color.rawValue)m\(self)\u{001B}[0;\(Color.default.rawValue)m"
	}
}
