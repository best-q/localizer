//
//  Constants.swift
//  localizer
//
//  Created by Admin.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

class Constants {
	static let json = "json"
	static let plist = "plist"
	static let maxWaitTime = 30.0
	static let tokenExpiredTime = 3600.0
	static let delimeter: Character = "_"
	static let localizableFile: String = "Localizable.strings"
	static let stringsStruct: String = "Strings"
	static let pluralStruct: String = "Plural"
	static let defaultConfigFile = "sheet_config.json"
	static let defaultLangPattern = "-default"
	static let comment = "///This is file generated automatically. You shouldn't edit it manually\n"
	static let googleTokenUrl = "https://www.googleapis.com/oauth2/v4/token"
	static let sheetInfoUrl = "https://sheets.googleapis.com/v4/spreadsheets/%@?access_token=%@"
	static let sheetDataUrl = "https://sheets.googleapis.com/v4/spreadsheets/%@/values/%@!%@?access_token=%@"
	static let abc = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	static let googleGrandType = "urn:ietf:params:oauth:grant-type:jwt-bearer"
	static let googleScopes = [
		"https://www.googleapis.com/auth/spreadsheets",
		"https://www.googleapis.com/auth/spreadsheets.readonly",
		"https://www.googleapis.com/auth/drive",
		"https://www.googleapis.com/auth/drive.file",
		"https://www.googleapis.com/auth/drive.readonly",
	]

	static let escapeNames: [String] = [
		"return",
		"default"
	]

	static func mainProtocols(_ isPublic: Bool) -> String {
		let prefix = isPublic ? "public " : ""
		return """
		import Foundation

		\(prefix)protocol Localizable {
		func localize(for key: String, arguments: [CVarArg]) -> String
		}

		\(prefix)protocol Pluralize {
		func pluralize(for key: String, value: Int, arguments: [CVarArg]) -> String
		func pluralize(for key: String, value: Int, arguments: [CVarArg]) -> NSAttributedString
		}

		\(prefix)protocol Spannable {
		func attribute(for key: String, arguments: [CVarArg]) -> NSAttributedString
		}

		class BaseLocalizer: Localizable {
		func localize(for key: String, arguments: [CVarArg]) -> String {
		let key = NSLocalizedString(key, comment: "comment")
		return String(format: key, arguments: arguments)
		}
		}

		class BaseAttributizer: BaseLocalizer, Spannable {
		func attribute(for key: String, arguments: [CVarArg]) -> NSAttributedString {
		return NSAttributedString(string: self.localize(for: key, arguments: arguments))
		}
		}

		class BasePluralizer: BaseAttributizer, Pluralize {
		enum PluralType: String {
		case zero, one, two, few, many, other
		}
		func pluralize(for key: String, value: Int, arguments: [CVarArg]) -> String {
		let type = getPluralType(value)
		return localize(for: \"\\(key)_\\(type)", arguments: arguments.isEmpty ? [value] : arguments)
		}

		func pluralize(for key: String, value: Int, arguments: [CVarArg]) -> NSAttributedString {
		return NSAttributedString(string: self.pluralize(for: key, value: value, arguments: arguments.isEmpty ? [value] : arguments))
		}

		private func getPluralType(_ count: Int) -> PluralType {
		switch Locale.current.languageCode ?? "en" {
		case "ru": return countToRuPlural(count)
		default: return countToEnPlural(count)
		}
		}

		private func countToRuPlural(_ count: Int) -> PluralType {
		if count == 0 {
		return .zero
		}
		else {
		let rem100 = count % 100
		let rem10 = count % 10
		if rem10 == 1 && rem100 != 11 {
		return .one
		}
		else if (2...4).contains(rem10) && !(12...14).contains(rem100) {
		return .few
		}
		else if rem10 == 0 || (5...9).contains(rem10) || (11...14).contains(rem10) {
		return .many
		}
		else {
		return .other
		}
		}
		}

		private func countToEnPlural(_ count: Int) -> PluralType {
		switch count {
		case 0: return .zero
		case 1: return .one
		default: return .many
		}
		}
		}

		\(prefix)struct Localization {
		fileprivate(set) \(prefix)static var localizer: Localizable = BaseLocalizer()
		fileprivate(set) \(prefix)static var pluralizer: Pluralize = BasePluralizer()
		fileprivate(set) \(prefix)static var attributizer: Spannable!

		\(prefix)static func setLocalizer(localizer: Localizable) {
		Localization.localizer = localizer
		}

		\(prefix)static func setPluralizer(pluralizer: Pluralize) {
		Localization.pluralizer = pluralizer
		}

		\(prefix)static func setSpannable(attributizer: Spannable?) {
		Localization.attributizer = attributizer
		}
		"""
	}
}
