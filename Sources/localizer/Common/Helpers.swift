//
//  Helpers.swift
//  localizer
//
//  Created by Admin.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

struct DynamicIntStringKey: CodingKey {
	var intValue: Int?
	var stringValue: String

	init?(intValue: Int) {
		self.intValue = intValue
		self.stringValue = "\(intValue)"
	}

	init?(stringValue: String) {
		self.stringValue = stringValue
	}
}

func safety(message: String, action: @escaping () throws -> Void) {
	do {
		try action()
	}
	catch {
		print(error: "\(message): \(error.localizedDescription)")
		exit(EXIT_FAILURE)
	}
}

func readLine(message: String = "") -> String? {
	if !message.isEmpty {
		print(message, separator: " ", terminator: "")
	}
	return readLine(strippingNewline: message.isEmpty)?.trimmingCharacters(in: .whitespacesAndNewlines)
}

func print(error: String) {
	print("\("Error".color(.red)): \(error)")
}

func print(success: String) {
	print(success.color(.green))
}

func print(info: String) {
	print(info.color(.blue))
}

func isEmpty(_ string: String?) -> Bool {
	return string == nil || string?.isEmpty ?? true
}

func parse<T: Codable>(_ type: T.Type, jsonFile: URL) -> T {
	do {
		let jsonData = try Data(contentsOf: jsonFile, options: .mappedIfSafe)
		return parse(type, jsonData: jsonData)
	}
	catch {
		print(error: "Parse error: \(error.localizedDescription)")
		exit(EXIT_FAILURE)
	}
}

func parse<T: Codable>(_ type: T.Type, jsonData: Data, fromGoogle: Bool = false) -> T {
	do {
		return try JSONDecoder().decode(type, from: jsonData)
	}
	catch {
		if fromGoogle, let errorItem = try? JSONDecoder().decode(GoogleErrorEntity.self, from: jsonData) {
			print(error: "Google error: \(errorItem.error.description)")
		}
		else {
			print(error: "Parse error: \(error.localizedDescription)")
		}
		exit(EXIT_FAILURE)
	}
}
