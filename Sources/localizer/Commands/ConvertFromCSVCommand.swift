//
//  ConvertFromCSVCommand.swift
//  localizer
//
//  Created by Admin on 17.06.2021.
//  Copyright © 2021 Admin. All rights reserved.
//

import Foundation
import CSV

class ConvertFromCSVCommand: ConsoleCommandProtocol {
    
    let name: String = "convert-csv"
    let description: String = """
    Params: [CSV file path] [output JSON file]
    Get language table data from CSV file by selected config and save as JSON file.
    For sample configuration file see this README.md
    """

    private var inputFilePath: URL?
    private var outputFilePath: URL?
    private var configPath: URL? {
        didSet {
            guard let configPath = configPath,
                  configPath.hasDirectoryPath,
                  configPath.pathExtension != Constants.json else {
                return
            }
            print(error: "Configuration file path not a JSON file.")
            exit(EXIT_FAILURE)
        }
    }

    func perform(arguments: [String]) {
        parsingParams(arguments: arguments)

        guard let inputFilePath = inputFilePath,
              let outputFilePath = outputFilePath,
              let configFilePath = configPath else {
            print(error: "Configuration, input or output file is not specified.")
            exit(EXIT_FAILURE)
        }

        let config: CSVTableEntity = parse(CSVTableEntity.self, jsonFile: configFilePath)

        let stream = InputStream(fileAtPath: inputFilePath.relativeString)!
        let csv = try! CSVReader(stream: stream, hasHeaderRow: true, trimFields: false, delimiter: ";")

        guard let header = csv.headerRow else {
            print(error: "Empty CSV file")
            exit(EXIT_FAILURE)
        }

        let availablePlatforms = [config.commonPlatformName, config.currentPlatformName]
        let allSpecifiedColumns = config.columns.getAll()
        let languageIndexes: [String: Int] = header
            .reduce(into: [String: Int?](), { dict, name in dict[name] = !allSpecifiedColumns.contains(name) ? header.firstIndex(where: { $0 == name }) : nil })
            .compactMapValues({ $0 })

        let defaultLanguage = languageIndexes.keys.first(where: { $0.contains(Constants.defaultLangPattern)})

        guard let excludeIndex: Int = header.firstIndex(where: { $0 == config.columns.exclude }),
              let keyIndex: Int = header.firstIndex(where: { $0 == config.columns.key }),
              let platformIndex: Int = header.firstIndex(where: { $0 == config.columns.platform }),
              let quantityIndex: Int = header.firstIndex(where: { $0 == config.columns.quantity }),
              let spannableIndex: Int = header.firstIndex(where: { $0 == config.columns.spannable }),
              let commentIndex: Int = header.firstIndex(where: { $0 == config.columns.comment }),
              let defaultLanguageIndex: Int = header.firstIndex(where: { $0 == defaultLanguage?.value }) else {
                  print(error: "Table header mapping error.")
                  exit(EXIT_FAILURE)
              }

        var tableMatrix: [[String]] = []

        while let row = csv.next() {
            tableMatrix.append(row)
        }

        tableMatrix = tableMatrix.filter({
            // not exclude row
            $0[safe: excludeIndex].isEmpty &&
                // no empty key
                !$0[safe: keyIndex].isEmpty &&
                // match platform
                !$0[safe: platformIndex].isEmpty &&
                availablePlatforms.contains($0[safe: platformIndex])

        }).sorted(by: { r, _ in r[safe: platformIndex] == config.commonPlatformName })

        var plurals = Dictionary(uniqueKeysWithValues: languageIndexes.map { ($0.key, Array<LocalizationSequence<PluralEntity>>()) })
        var strings = Dictionary(uniqueKeysWithValues: languageIndexes.map { ($0.key, Array<LocalizationSequence<StringEntity>>()) })

        for row in tableMatrix {
            guard let key = row[safeNullable: keyIndex]?.removeAllSpaces() else {
                return
            }
            let spannable = Bool(row[safe: spannableIndex].lowercased()) ?? false
            let languageData = languageIndexes.reduce(into: [String: String](), { (dict, item) in dict[item.key] = row[safe: item.value] })

            let comment = row[safe: commentIndex]
            let defaultLanguageValue = row[safe: defaultLanguageIndex]

            var pluralType: PluralType?
            if let quantity = row[safeNullable: quantityIndex] {
                pluralType = PluralType(rawValue: quantity)
            }

            for tableRow in languageData {
                if let plural = pluralType {
                    let plural = PluralEntity(fullName: key.value, types: [plural: tableRow.value.isEmpty ? defaultLanguageValue : tableRow.value], spannable: spannable, comment: comment)
                    let sequence = LocalizationSequence<PluralEntity>.create(resource: plural)
                    plurals[tableRow.key] = merge(items: plurals[tableRow.key] ?? [], sequence: sequence)
                }
                else {
                    let string = StringEntity(fullName: key.value, value: tableRow.value.isEmpty ? defaultLanguageValue : tableRow.value, spannable: spannable, comment: comment)
                    let sequence = LocalizationSequence<StringEntity>.create(resource: string)
                    strings[tableRow.key] = merge(items: strings[tableRow.key] ?? [], sequence: sequence)
                }
            }
        }

        let model = Localization(languages: languageIndexes.map({

            Language(
                name: $0.key.replacingOccurrences(of: Constants.defaultLangPattern, with: ""),
                isDefault: $0.key.contains(Constants.defaultLangPattern),
                strings: strings[$0.key] ?? [],
                plurals: plurals[$0.key] ?? []
            )
        }))

        safety(message: "Localization file save error") {
            let ext = outputFilePath.pathExtension
            guard let format = LocalizationProvider.Format(rawValue: ext) else {
                print(error: "Output file has an invalid extension. Valid JSON and PLIST.")
                exit(EXIT_FAILURE)
            }
            let res = LocalizationProvider().save(model: model, format: format, file: outputFilePath)
            switch res {
            case .success:
                print(success: "'\(outputFilePath.path)' was created successfully.")
            case .failure(let error):
                print(error: error.localizedDescription)
                exit(EXIT_FAILURE)
            }
        }
    }

    private func parsingParams(arguments: [String]) {
        if arguments.count == 0 || arguments.count == 1 {
            print(error: "Input and Output files are not specified.")
            exit(EXIT_FAILURE)
        }
        else if arguments.count == 2 {
            inputFilePath = URL(fileURLWithPath: arguments[0])
            outputFilePath = URL(fileURLWithPath: arguments[1])
            configPath = getConfigFileFromCWD()
        } else if arguments.count == 2 {
            inputFilePath = URL(fileURLWithPath: arguments[0])
            outputFilePath = URL(fileURLWithPath: arguments[1])
            configPath = URL(fileURLWithPath: arguments[2])
        } else {
            print(error: "Too many arguments.")
            exit(EXIT_FAILURE)
        }
    }

    private func getConfigFileFromCWD() -> URL {
        let defaultConfigPath = URL(fileURLWithPath: fileManager.currentDirectoryPath)
            .appendingPathComponent(Constants.defaultConfigFile)
        guard fileManager.fileExists(atPath: defaultConfigPath.path) else {
            print(error: "Config file '\(defaultConfigPath.path)' not found.")
            print("  - see README.md for example")
            exit(EXIT_FAILURE)
        }
        return defaultConfigPath
    }

    private func merge<T>(items: [LocalizationSequence<T>], sequence: LocalizationSequence<T>) -> [LocalizationSequence<T>] where T: LocalizationEntity {
        var isMerged = false

        var res = items

        for (index, var item) in items.enumerated() {
            if item.merge(sequence: sequence) {
                res[index] = item
                isMerged = true
                break
            }
        }
        if !isMerged {
            res.append(sequence)
        }
        return res
    }
}
