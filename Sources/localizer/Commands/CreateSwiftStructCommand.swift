//
//  GenerateSwiftCommand.swift
//  localizer
//
//  Created by Admin.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

class CreateSwiftStructCommand: ConsoleCommandProtocol {

	var name: String = "gen-swift-struct"
	var description: String = """
Params: -P [json/plist file path] [struct file path]
  -P - make public
"""

	private var isPublic: Bool = false
	private var enumPath: URL?
	private var localizationPath: URL?

	func perform(arguments: [String]) {
		parseArguments(arguments: arguments)

		guard let enumPath = enumPath,
			let localizationPath = localizationPath else {
				print(error: "Enum and localizable files is not specified.")
				exit(EXIT_FAILURE)
		}
		let res = LocalizationProvider().load(filePath: localizationPath)

		switch res {
		case .failure(let error):
			print(error: error.localizedDescription)
			exit(EXIT_FAILURE)
		case let .success((_, localization)):
			makeLocalizationFile(localization, at: enumPath)
		}
	}

	private func parseArguments(arguments: [String]) {
		var args = arguments
		args.removeAll(where: { arg in
			let result = arg == "-P"
			if result {
				self.isPublic = true
			}
			return result
		})
		if args.count < 2 {
			print(error: "Incorrect Params.")
			exit(EXIT_FAILURE)
		}
		else if args.count == 2 {
			localizationPath = URL(fileURLWithPath: args[0])
			enumPath = URL(fileURLWithPath: args[1])
		}
		else {
			print(error: "Too many arguments.")
			exit(EXIT_FAILURE)
		}
	}

	private func makeLocalizationFile(_ localization: Localization, at localizationPath: URL) {
		guard let language = localization.languages.first(where: {$0.isDefault == true}), (language.strings.count > 0 || language.plurals.count > 0) else {
			print(error: "Input localization file is empty.")
			exit(EXIT_FAILURE)
		}

		var swiftFileString: String = ""
		swiftFileString += Constants.comment
		swiftFileString += "\n"
		swiftFileString += Constants.mainProtocols(isPublic)


		swiftFileString += "\n"
		if language.strings.count > 0 {
			swiftFileString += beginStruct(Constants.stringsStruct)

            swiftFileString += language.strings.sorted(by: { $0.name < $1.name }).reduce("", { $0 + self.makeBody(sequence: $1) })

			swiftFileString += endStruct()
		}

		if language.plurals.count > 0 {
			swiftFileString += beginStruct(Constants.pluralStruct)

            swiftFileString += language.plurals.sorted(by: { $0.name < $1.name }).reduce("", { $0 + self.makeBody(sequence: $1) })

			swiftFileString += endStruct()
		}

		swiftFileString += endStruct(level: -1)

		let savePath = localizationPath.path
		safety(message: "Enum file save error") {
			try swiftFileString.write(toFile: savePath, atomically: true, encoding: .utf8)
			print(success: "'\(savePath)' was created successfully.")
		}
	}

	private func beginStruct(_ name: String, level: Int = 0) -> String {
		return "\(isPublic ? "public " : "")struct \(name.capitalizingFirstLetter()) {".tab(1 + level)
	}

	private func endStruct(level: Int = 0) -> String {
		return "}\n".tab(1 + level)
	}


	private func makeBody<T>(sequence: LocalizationSequence<T>, level: Int = 1) -> String {
		var res = "\n"
		res += beginStruct(sequence.name).tab(level) + "\n"
        for child in sequence.children.sorted(by: { $0.name < $1.name }) {
			res += makeBody(sequence: child, level: level + 1)
		}

        for resource in sequence.resources.sorted(by: { $0.fullName < $1.fullName }) {
			res += makeResource(resource, level: level)
		}
		res += endStruct().tab(level) + "\n"
		return res
	}

	private func makeResource(_ resource: LocalizationEntity, level: Int) -> String {
		let offset = 2
		var comment = "\n"
		var item = "\(isPublic ? "public " : "")static func \(escape(name: resource.name))"
		if let resource = resource as? StringEntity {
			item += "(_ arguments: [CVarArg] = []) -> "
			comment = "/** \(resource.comment ?? resource.value.replacingOccurrences(of: "\n", with: "\\n")) */"
			if resource.spannable {
				item += "NSAttributedString { Localization.attributizer.attribute(for: \"\(resource.fullName)\", arguments: arguments)}"
			}
			else {
				item += "String { Localization.localizer.localize(for: \"\(resource.fullName)\", arguments: arguments)}"
			}
		}
		else if let resource = resource as? PluralEntity {
			item += "(_ count: Int, _ arguments: [CVarArg] = []) -> "
			comment = "/** \(resource.comment ?? resource.fullName) */"
			if resource.spannable {
				item += "NSAttributedString { Localization.pluralizer.pluralize(for: \"\(resource.fullName)\", value: count, arguments: arguments)}"
			}
			else {
				item += "String { Localization.pluralizer.pluralize(for: \"\(resource.fullName)\", value: count, arguments: arguments)}"
			}
		}
		else {
			fatalError("Couldn't generate resource for \(resource.fullName)")
		}
		print(resource.fullName.color(.green))
		return comment.tab(offset + level) + "\n" + item.tab(level + offset) + "\n"
	}

	private func escape(name: String) -> String {
		if Constants.escapeNames.contains(name) {
			return "`\(name)`"
		}
		return name
	}

}
