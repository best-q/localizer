//
//  ConvertCommand.swift
//  localizer
//
//  Created by Admin.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

class ConvertSourceCommand: ConsoleCommandProtocol {
	let name: String = "convert"
	let description: String = """
		Params: [input] [output]
		Convert input localization to output file path with json/plist type.
		"""
	private var inputPath: URL?
	private var outputPath: URL?

	func perform(arguments: [String]) {
		parseArguments(arguments: arguments)

		guard let inputPath = inputPath,
			let outputPath = outputPath else {
				print(error: "Invalid paths.")
				exit(EXIT_FAILURE)
		}

		var isJSON = true

		var localization: Localization?

		do {
			guard let jsonData = try String(contentsOf: inputPath).data(using: .utf8) else {
				throw ""
			}
			localization = try JSONDecoder().decode(Localization.self, from: jsonData)
			isJSON = true
		}
		catch {}

		if localization == nil {
			do {
				let data = try Data(contentsOf: inputPath)
				localization = try PropertyListDecoder().decode(Localization.self, from: data)
				isJSON = false
			}
			catch {}
		}

		guard let localizationData = localization else {
			print(error: "'\(inputPath.path)' parsing error (or file is not json or plist).")
			exit(EXIT_FAILURE)
		}

		safety(message: "Localization file save error") {
			if !isJSON {
				let encoder = JSONEncoder()
				encoder.outputFormatting = .prettyPrinted
				let data = try encoder.encode(localizationData)
				guard let dataString = String(data: data, encoding: .utf8) else {
					print(error: "Invalid 'Data -> String' converting.")
					exit(EXIT_FAILURE)
				}
				try dataString.write(toFile: outputPath.path, atomically: true, encoding: .utf8)
				print(success: "'\(outputPath.path)' was created successfully.")
			}
			else {
				let encoder = PropertyListEncoder()
				encoder.outputFormat = .xml
				let data = try encoder.encode(localizationData)
				try data.write(to: outputPath)
				print(success: "'\(outputPath.path)' was created successfully.")
			}
		}
	}

	private func parseArguments(arguments: [String]) {
		if arguments.count < 2 {
			print(error: "Incorrect Params.")
			exit(EXIT_FAILURE)
		}
		else if arguments.count == 2 {
			inputPath = URL(fileURLWithPath: arguments[0])
			outputPath = URL(fileURLWithPath: arguments[1])
		}
		else {
			print(error: "Too many arguments.")
			exit(EXIT_FAILURE)
		}
	}
}
