//
//  LoadCommand.swift
//  localizer
//
//  Created by Admin.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

class LoadFromGoogleTableCommand: ConsoleCommandProtocol {
	
	let name: String = "load"
	let description: String = """
	Params: [output JSON file] ([config file path] if needs)
	Get language table data from Google Sheets by selected config and save as JSON file.
	If configuration file is not specified in Params, it will be taken from CWD as '\(Constants.defaultConfigFile)'.
	For sample configuration file see this README.md
	"""
	
	private var outputFilePath: URL?
	private var configPath: URL? {
		didSet {
			guard let configPath = configPath,
				configPath.hasDirectoryPath,
				configPath.pathExtension != Constants.json else {
					return
			}
			print(error: "Configuration file path not a JSON file.")
			exit(EXIT_FAILURE)
		}
	}
	
	func perform(arguments: [String]) {
		parsingParams(arguments: arguments)
		
		guard let configFilePath = configPath,
			let outputFilePath = outputFilePath else {
				print(error: "Configuration or output file is not specified.")
				exit(EXIT_FAILURE)
		}
		
		let config: SheetEntity = parse(SheetEntity.self, jsonFile: configFilePath)
		
		var tableMatrix = getTableData(config: config)
		let header: [String] = tableMatrix.removeFirst()
		
		guard let excludeIndex: Int = header.firstIndex(where: { $0 == config.columns.exclude }),
			let keyIndex: Int = header.firstIndex(where: { $0 == config.columns.key }),
			let platformIndex: Int = header.firstIndex(where: { $0 == config.columns.platform }),
			let quantityIndex: Int = header.firstIndex(where: { $0 == config.columns.quantity }),
			let spannableIndex: Int = header.firstIndex(where: { $0 == config.columns.spannable }),
			let commentIndex: Int = header.firstIndex(where: { $0 == config.columns.comment }) else {
				print(error: "Table header mapping error.")
				exit(EXIT_FAILURE)
		}
		
		let availablePlatforms = [config.commonPlatformName, config.currentPlatformName]
		let allSpecifiedColumns = config.columns.getAll()
		let languageIndexes: [String: Int] = header
			.reduce(into: [String: Int?](), { dict, name in dict[name] = !allSpecifiedColumns.contains(name) ? header.firstIndex(where: { $0 == name }) : nil })
			.compactMapValues({ $0 })
		
		tableMatrix = tableMatrix.filter({
			// not exclude row
			$0[safe: excludeIndex].isEmpty &&
				// no empty key
				!$0[safe: keyIndex].isEmpty &&
				// match platform
				!$0[safe: platformIndex].isEmpty &&
				availablePlatforms.contains($0[safe: platformIndex])
			
		}).sorted(by: { r, _ in r[safe: platformIndex] == config.commonPlatformName })
		
		var plurals = Dictionary(uniqueKeysWithValues: languageIndexes.map { ($0.key, Array<LocalizationSequence<PluralEntity>>()) })
		var strings = Dictionary(uniqueKeysWithValues: languageIndexes.map { ($0.key, Array<LocalizationSequence<StringEntity>>()) })
		
		for row in tableMatrix {
			guard let key = row[safeNullable: keyIndex]?.removeAllSpaces() else {
				return
			}
			let spannable = Bool(row[safe: spannableIndex].lowercased()) ?? false
			let languageData = languageIndexes.reduce(into: [String: String](), { (dict, item) in dict[item.key] = row[safe: item.value] })
			let comment = row[safe: commentIndex]
			var pluralType: PluralType?
			if let quantity = row[safeNullable: quantityIndex] {
				pluralType = PluralType(rawValue: quantity)
			}
			
			
			for tableRow in languageData {
				if let plural = pluralType {
					let plural = PluralEntity(fullName: key.value, types: [plural: tableRow.value], spannable: spannable, comment: comment)
					let sequence = LocalizationSequence<PluralEntity>.create(resource: plural)
					plurals[tableRow.key] = merge(items: plurals[tableRow.key] ?? [], sequence: sequence)
				}
				else {
					let string = StringEntity(fullName: key.value, value: tableRow.value, spannable: spannable, comment: comment)
					let sequence = LocalizationSequence<StringEntity>.create(resource: string)
					strings[tableRow.key] = merge(items: strings[tableRow.key] ?? [], sequence: sequence)
				}
			}
		}
		
		let model = Localization(languages: languageIndexes.map({
			Language(
				name: $0.key.replacingOccurrences(of: Constants.defaultLangPattern, with: ""),
				isDefault: $0.key.contains(Constants.defaultLangPattern),
				strings: strings[$0.key] ?? [],
				plurals: plurals[$0.key] ?? []
			)
		}))
		
		safety(message: "Localization file save error") {
			let ext = outputFilePath.pathExtension
			guard let format = LocalizationProvider.Format(rawValue: ext) else {
				print(error: "Output file has an invalid extension. Valid JSON and PLIST.")
				exit(EXIT_FAILURE)
			}
			let res = LocalizationProvider().save(model: model, format: format, file: outputFilePath)
			switch res {
			case .success:
				print(success: "'\(outputFilePath.path)' was created successfully.")
			case .failure(let error):
				print(error: error.localizedDescription)
				exit(EXIT_FAILURE)
			}
		}
	}
	
	private func merge<T>(items: [LocalizationSequence<T>], sequence: LocalizationSequence<T>) -> [LocalizationSequence<T>] where T: LocalizationEntity {
		var isMerged = false
		
		var res = items
		
		for (index, var item) in items.enumerated() {
			if item.merge(sequence: sequence) {
				res[index] = item
				isMerged = true
				break
			}
		}
		if !isMerged {
			res.append(sequence)
		}
		return res
	}
	
	
	private func getTableData(config: SheetEntity) -> [[String]] {
		let group = DispatchGroup()
		var auth: GoogleTokenEntity?
		var propertiesItem: SheetInfoProperties?
		var tableDataItem: SheetData?
		
		group.enter()
		let authRequest = RequestManager.create(
			.post,
			to: URL(string: Constants.googleTokenUrl)!,
			Params: [
				"grant_type": Constants.googleGrandType,
				"assertion": config.credentials.getAccessToken()
			],
			contentType: "application/x-www-form-urlencoded"
		)
		load(urlRequest: authRequest) { data in
			auth = parse(GoogleTokenEntity.self, jsonData: data, fromGoogle: true)
			group.leave()
		}
		
		guard let accessToken = auth?.accessToken else {
			print(error: "Invalid access token.")
			exit(EXIT_FAILURE)
		}
		
		let infoUrl = URL(string: String(format: Constants.sheetInfoUrl, config.spreadsheetId, accessToken))!
		group.enter()
		load(urlRequest: RequestManager.create(.get, to: infoUrl)) { infoData in
			let sheetInfo: SheetInfo = parse(SheetInfo.self, jsonData: infoData, fromGoogle: true)
			sheetInfo.sheets.forEach { item in
				if item.properties.title == config.listName {
					propertiesItem = item.properties
				}
			}
			group.leave()
		}
		
		guard let tableProperties = propertiesItem else {
			print(error: "Selected page not found.")
			exit(EXIT_FAILURE)
		}
		
		let tableRange = self.calculateTableRange(gridProperties: tableProperties.gridProperties)
		let dataUrl = URL(string: String(format: Constants.sheetDataUrl, config.spreadsheetId, config.listName, tableRange, accessToken))!
		group.enter()
		load(urlRequest: RequestManager.create(.get, to: dataUrl)) { data in
			tableDataItem = parse(SheetData.self, jsonData: data, fromGoogle: true)
			group.leave()
		}
		
		guard let tableMatrix = tableDataItem?.values else {
			print(error: "Table is empty.")
			exit(EXIT_FAILURE)
		}
		
		return tableMatrix
	}
	
	private func calculateTableRange(gridProperties: SheetInfoGridProperties) -> String {
		return "A1:\(gridProperties.columnCount.toColumnIndex())\(gridProperties.rowCount)"
	}
	
	private func load(urlRequest: URLRequest, completion: @escaping (Data) -> Void) {
		let defaultSession = URLSession(configuration: .default)
		let flag = DispatchSemaphore(value: 0)
		let task = defaultSession.dataTask(with: urlRequest) { data, _, error in
			if let error = error {
				print(error: error.localizedDescription)
				exit(EXIT_FAILURE)
			}
			else if let data = data {
				completion(data)
			}
			else {
				print(error: "\(String(describing: urlRequest.url?.path)) get request error.")
				exit(EXIT_FAILURE)
			}
			flag.signal()
		}
		task.resume()
		_ = flag.wait(timeout: DispatchTime.now() + urlRequest.timeoutInterval)
	}
	
	private func parsingParams(arguments: [String]) {
		if arguments.count == 0 {
			print(error: "Output file is not specified.")
			exit(EXIT_FAILURE)
		}
		else if arguments.count == 1 {
			outputFilePath = URL(fileURLWithPath: arguments[0])
			configPath = getConfigFileFromCWD()
		}
		else if arguments.count == 2 {
			outputFilePath = URL(fileURLWithPath: arguments[0])
			configPath = URL(fileURLWithPath: arguments[1])
		}
		else {
			print(error: "Too many arguments.")
			exit(EXIT_FAILURE)
		}
	}
	
	private func getConfigFileFromCWD() -> URL {
		let defaultConfigPath = URL(fileURLWithPath: fileManager.currentDirectoryPath)
			.appendingPathComponent(Constants.defaultConfigFile)
		guard fileManager.fileExists(atPath: defaultConfigPath.path) else {
			print(error: "Config file '\(defaultConfigPath.path)' not found.")
			print("  - see README.md for example")
			exit(EXIT_FAILURE)
		}
		return defaultConfigPath
	}
	
}

