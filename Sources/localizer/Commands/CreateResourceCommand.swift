//
//  CreateResourceCommand.swift
//  localizer
//
//  Created by Admin.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

class CreateResourceCommand: ConsoleCommandProtocol {
	
	var name: String = "gen-resource-files"
	var description: String = """
	Params: -P [json/plist file path] [localization path] [file name default is \(Constants.localizableFile)]
	Generate localizable string resources
	"""
	
	private var sourcePath: URL?
	private var destinationFolder: URL?
	private var fileName: String = Constants.localizableFile
	let regex: NSRegularExpression = {
		return try! NSRegularExpression(pattern: "\\$(\\d+)")
	}()
	
	func perform(arguments: [String]) {
		parseArguments(arguments: arguments)
		
		guard let sourcePath = sourcePath,
			let destinationFolder = destinationFolder else {
				print(error: "Enum and localizable files is not specified.")
				exit(EXIT_FAILURE)
		}
		let res = LocalizationProvider().load(filePath: sourcePath)
		
		switch res {
		case .failure(let error):
			print(error: error.localizedDescription)
			exit(EXIT_FAILURE)
		case let .success((_, localization)):
			generateStringFiles(localization, at: destinationFolder)
		}
	}
	
	private func parseArguments(arguments: [String]) {
		if arguments.count < 2 {
			print(error: "Incorrect Params.")
			exit(EXIT_FAILURE)
		}
		else if arguments.count >= 2 {
			sourcePath = URL(fileURLWithPath: arguments[0])
			destinationFolder = URL(fileURLWithPath: arguments[1])
			
			if arguments.count > 2 {
				fileName = arguments[2]
			}
		}
		else {
			print(error: "Too many arguments.")
			exit(EXIT_FAILURE)
		}
	}
	
	private func generateStringFiles(_ localization: Localization, at path: URL) {
		for locale in localization.languages {
			var content = Constants.comment
			content += "\n"
			
            content += locale.strings.sorted(by: { $0.name < $1.name }).reduce("", { $0 + self.makeBody(sequence: $1) })
            content += locale.plurals.sorted(by: { $0.name < $1.name }).reduce("", { $0 + self.makeBody(sequence: $1) })
			
			var url = path.appendingPathComponent("\(locale.name).lproj", isDirectory: true)
			let manager = FileManager.default
			do {
				try manager.createDirectory(at: url, withIntermediateDirectories: true, attributes: nil)
				url.appendPathComponent(Constants.localizableFile, isDirectory: false)
				try? manager.removeItem(at: url)
				
				guard manager.createFile(atPath: url.path, contents: content.data(using: .utf8), attributes: nil) else {
					print(error: "Could not created \(url.path)")
					exit(EXIT_FAILURE)
				}
				print(info: "Created: \(url.path)")
			}
			catch {
				print(error: error.localizedDescription)
			}
		}
		print(success: "Done")
	}
	
	private func makeBody<T>(sequence: LocalizationSequence<T>) -> String {
		var res = ""
		
        for child in sequence.children.sorted(by: { $0.name < $1.name }) {
			res += makeBody(sequence: child)
		}
		
        for resource in sequence.resources.sorted(by: { $0.fullName < $1.fullName }) {
			res += makeResource(resource)
		}
		return res
	}
	
	private func makeResource(_ resource: LocalizationEntity) -> String {
		var res = ""
		if let resource = resource as? StringEntity {
			res = "\"\(resource.fullName)\" = \"\(escape(resource.value))\";\n"
		}
		else if let resource = resource as? PluralEntity {
            for row in resource.types.sorted(by: { $0.key.rawValue < $1.key.rawValue }) {
				res += "\"\(resource.fullName)\(Constants.delimeter)\(row.key)\" = \"\(escape(row.value))\";\n"
			}
		}
		else {
			fatalError("Couldn't generate resource for \(resource.fullName)")
		}
		print(resource.fullName.color(.green))
		return res
	}
	
	private func escape(_ value: String) -> String {
		var res = value
		res = res.replacingOccurrences(of: "\n", with: "\\n")
		res = res.replacingOccurrences(of: "\"", with: "\\\"")
		
		res = regex.stringByReplacingMatches(in: res, range: NSRange(value.startIndex..., in: res), withTemplate: "%$1$@")
		return res
	}
	
}

