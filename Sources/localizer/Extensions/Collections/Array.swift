//
//  Array.swift
//  localizer
//
//  Created by Admin.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

extension Array where Element == String {
	subscript(safe index: Int) -> Element {
		return self.indices.contains(index) ? self[index] : String()
	}
	
	subscript(safeNullable index: Int) -> Element? {
		return self.indices.contains(index) ? self[index] : nil
	}
}
