//
//  Dictionary.swift
//  localizer
//
//  Created by Admin.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

extension Dictionary {
	func percentEscaped() -> String {
		return self.map({ (key, value) in
			let escapedKey = "\(key)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
			let escapedValue = "\(value)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
			return escapedKey + "=" + escapedValue
		}).joined(separator: "&")
	}

	subscript(keyPath keyPath: String) -> Any? {
		get {
			guard let keyPath = Dictionary.keyPathKeys(forKeyPath: keyPath)
				else { return nil }
			return getValue(forKeyPath: keyPath)
		}
		set {
			guard let keyPath = Dictionary.keyPathKeys(forKeyPath: keyPath),
				let newValue = newValue
				else { return }
			self.setValue(newValue, forKeyPath: keyPath)
		}
	}

	static private func keyPathKeys(forKeyPath: String) -> [Key]? {
		let keys = forKeyPath.components(separatedBy: "_").reversed().compactMap({ $0.capitalizingFirstLetter() as? Key })
		return keys.isEmpty ? nil : keys
	}

	private func getValue(forKeyPath keyPath: [Key]) -> Any? {
		guard let lastKey = keyPath.last, let value = self[lastKey]
			else { return nil }
		return keyPath.count == 1 ? value : (value as? [Key: Any])
			.flatMap { $0.getValue(forKeyPath: Array(keyPath.dropLast())) }
	}

	private mutating func setValue(_ value: Any, forKeyPath keyPath: [Key]) {
		var keyPath = keyPath
		guard let firstKey = keyPath.popLast() else { return }
		if keyPath.isEmpty {
			self[firstKey] = value as? Value
		}
		else {
			var dict: [Key: Any] = self[firstKey] as? [Key: Any] ?? [:]
			deepSet(value, keyPath: keyPath, in: &dict)
			self[firstKey] = dict as? Value
		}
	}

	private func deepSet(_ value: Any, keyPath: [Key], in dict: inout [Key: Any]) {
		var keyPath = keyPath
		guard let key: Key = keyPath.popLast() else {
			return
		}
		if keyPath.isEmpty {
			dict[key] = value
			return
		}
		var bufferDict: [Key: Any] = dict[key] as? [Key: Any] ?? [:]
		deepSet(value, keyPath: keyPath, in: &bufferDict)
		dict[key] = bufferDict
	}
}
