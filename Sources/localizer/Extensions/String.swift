//
//  String.swift
//  localizer
//
//  Created by Admin.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

extension String: Error {}

extension String {
	func get(index: Int) -> String {
		return String(Array(self)[index])
	}

	func tab(_ count: Int) -> String {
		return String(repeating: "\t", count: count) + self
	}

	func removeAllSpaces() -> String {
		return self.replacingOccurrences(of: " ", with: "")
	}

	func capitalizingFirstLetter() -> String {
		guard count > 0 else { return "" }
		return prefix(1).uppercased() + self.dropFirst()
	}
}
