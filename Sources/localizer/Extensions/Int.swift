//
//  Int.swift
//  localizer
//
//  Created by Admin.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

extension Int {
	func toColumnIndex() -> String {
		return Int.toTableColumnIndex(self)
	}

	static func toTableColumnIndex(_ value: Int, buffer: String = "") -> String {
		let multiplier = value > Constants.abc.count
			? value / Constants.abc.count
			: 0
		if multiplier > 0 {
			let base: Int = value % Constants.abc.count - 1
			return toTableColumnIndex(multiplier, buffer: buffer) + Constants.abc.get(index: base)
		}
		else {
			return buffer + Constants.abc.get(index: value - 1)
		}
	}
}
